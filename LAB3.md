Лабораторная работа №3

В этой лабораторной работе вам необходимо осуществить примитивную программу шифрования в python.
Ваша программа должна будет генерировать случайный код подстановки для всех букв в диапазоне a-z, отображая их на другие буквы в a-z, чтобы буквы могли быть переведены обратно в исходные. 
Все остальные символы будут переданы без изменений. 

=> Необходимо написать два файла: 
1. `char_translator.py` Этот файл будет содержать код для генерации случайного кода замены для перевода и неперевода отдельных символов на основе этого кода. ПРИМЕЧАНИЕ: python теперь содержит строковые методы string.maketrans() и string.translate() в значительной степени делают это за вас. Поэтому не используйте ни один из этих методов, иначе проблема становится абсолютно тривиальной. 
2. `crypto.py` Этот файл будет выполнять фактический перевод и перенос файлов. 

=>1. Запишем код  в `char_translator.py`. 
=> Он будет содержать функцию `random_permute_chars():`,создающий случайную перестановку нижнего регистра:

    def random_permute_chars():    
        global alph
        random.shuffle(alph)
        return ''.join(alph)
        
=>2. Дплее инициализируем класс `InvalidTranslationString`, реализующая работу с исключением:

    class InvalidTranslationString(Exception):
        pass
        
=>3. Затем сам класс `CharTranslator`, реализущий работу шифратора и дешифратора и вызываемй в классе `Coder`
=> Шифруются символы в функции `encode_file()`по следующему алгоритму:
1. Вычисляется позиция символа в алфавите.
2. Из строки с перестановкой берется символ по полученному номеру позиции.
3. Заглавные буквы шифровать не обязательно, а прочие символы нужно оставить без изменений.

=>`decode_file` по не заданному алгоритму расшифровывает символы.

    class CharTranslator:
        def __init__(self, s):
            global alph
                if set(s) - set(alph):
            raise InvalidTranslationString
            self.alph = alph
            self.char_translator = s
        def translate_char(self, c):
            return self.char_translator[self.alph.index(c)]
        def untranslate_char(self, c):
            return self.alph[self.char_translator.index(c)]
            
    def encode_file(self, namein, nameout):
        try:
            fin = open(namein, 'r')
            fout = open(nameout, 'w')   
            for line in fin:
                if line == '':
                    break
                else:
                    print line
                    x = char_translator.CharTranslator(line)
                    s = ""
                    for ch in line:
                        s = s + x.translate_char(ch)
                    fout.write(s)
            fin.close()
            fout.close()
        except IOError as e:
            print 'No file found.'

    def decode_file(self, namein, nameout):
        try:
            fin = open(namein, 'r')
            fout = open(nameout, 'w')   
            for line in fin:
                if line == '':
                    break
                else:
                    x = char_translator.CharTranslator(line)
                    s = ""
                    for ch in line:
                        s = s + x.untranslate_char(ch)
                    fout.write(s)
            fin.close()
            fout.close()
        except IOError as e:
            print 'No file found.'